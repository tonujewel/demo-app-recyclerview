package com.rana.jewel.demoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private  RecyclerAdapter recyclerAdapter;
    private List<DataDM> movieList = new ArrayList<>();
    private Button btnAdd;
    private int position = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnAdd = (Button)findViewById(R.id.btnAdd);

        recyclerAdapter = new RecyclerAdapter(movieList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);


        DataDM dataDM = new DataDM();
        dataDM.setTitle("Do you like football ?");
        movieList.add(dataDM);

        DataDM dataDM2 = new DataDM();
        dataDM2.setTitle("Are you an Indian ?");
        movieList.add(dataDM2);

        DataDM dataDM3 = new DataDM();
        dataDM3.setTitle("Do You love Chinese foor ?");
        movieList.add(dataDM3);

        DataDM dataDM4 = new DataDM();
        dataDM4.setTitle("Do youe love Pizza ?");
        movieList.add(dataDM4);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position++;
                DataDM dataDM4 = new DataDM();
                dataDM4.setTitle("Do youe love Pizza ?"+position);
                movieList.add(dataDM4);
                recyclerAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
