package com.rana.jewel.demoapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;


/**
 * Created by Jewel Rana on 3/3/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{

    List<DataDM> movieDMList;
    
    public RecyclerAdapter(List<DataDM> movieDMList) {
        this.movieDMList = movieDMList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_list_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        DataDM dataDM = movieDMList.get(position);
        holder.textviewTitle.setText(dataDM.getTitle());
        holder.btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"Yes "+ position, Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"No "+ position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieDMList.size();
    }


    //........................Holder..........................//

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView textviewTitle;
        private Button btnYes,btnNo;

        public MyViewHolder(View itemView) {
            super(itemView);

            textviewTitle = (TextView) itemView.findViewById(R.id.textviewTitle);
            btnYes = (Button)itemView.findViewById(R.id.btnYes);
            btnNo = (Button)itemView.findViewById(R.id.btnNo);
        }
    }

    @Override
    public int getItemViewType(int position) {

        return super.getItemViewType(position);
    }


}
